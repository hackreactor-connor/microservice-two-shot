# from django.urls import path
# from .views import list_shoes, show_shoes

# urlpatterns = [
#     path("shoes/", list_shoes, name="list_shoes"),
#     path("shoes/<int:pk>/", show_shoes, name="show_shoe"),
#     path("shoes/", list_shoes, name="create_shoes"),
# ]
from django.urls import path
from .views import list_shoes, show_shoe

urlpatterns = [
    path("shoes/", list_shoes, name="list_shoes"),
    path("shoes/<int:pk>/", show_shoe, name="show_shoe"),

]
