# Wardrobify

Team:

* Connor Somers - Hats Microservice
* Harry Zeng - Shoes Microservice

## Design
![design](ghi/app/public/image.png)

## Shoes microservice

There will be a Shoe model in django. inside the app, there have a  manufacturer, model name, color, image, and the bin. and the bin will show where it the data located in. The data will be coming from the RESTful API and plugged into the react app.

## Hats microservice

In the hats microservice ther are two models, the actual "Hat" model, and a "LocationVO". The hat model has a foreign key with the LocationVO model which is a value object which represents which location the hat is stored in, and is updated with the poller which periodicially checks for changes between the location VO and the actual Location data.

## Setup

### Step 1

clone the repo to your local machine with:
    git clone https://gitlab.com/hackreactor-connor/microservice-two-shot.git

### Step 2

from the root of the project, run the following command(s)
    docker-compose build
    docker-compose up

if the database is not created, you may need to change the last line in the docker-compose.yml file
    from:
        external: true
    to:
        external: false
this is because the external volume sometimes will not be created. if you do do this, run: 'docker-compose up' again. You can change the line back after you get the initial build working

### Step 3

now, for each of the 3 api backends, you will want to initialize the database on each. you will want to use docker desktop to access the terminal for each container and run the following commands in each:
    python manage.py makemigrations
    python manage.py migrate

At this point, everythign should be working just fine, and you can access the react SPA at http://localhost:3000

### Insomnia request bodies and endpoints


#### hats
http://localhost:8090/api/hats/
http://localhost:8090/api/hats/1/

POST
{
"fabric": "velvet",
"style_name": "style1",
"color": "red",
"picture_url": "https://media.istockphoto.com/id/1184522745/photo/rodeo-horse-rider-wild-west-culture-americana-and-american-country-music-concept-theme-with-a.jpg?s=612x612&w=0&k=20&c=nQ5A-1FOuIvujY6AObkJ9xntyhfASTB",
"location": "api/hats/1/"
}




#### shoes
http://localhost:8080/api/shoes/
http://localhost:8080/api/shoes/1/

POST
{
	"manufacturer": "Terry",
	"model_name": "New",
	"color": "black",
	"picture_url": "https://media.istockphoto.com/id/1184522745/photo/rodeo-horse-rider-wild-west-culture-americana-and-american-country-music-concept-theme-with-a.jpg?s=612x612&w=0&k=20&c=nQ5A-1FOuIvujY6AObkJ9xntyhfASTB",
	"bin": "api/shoes/1/"
}



#### wardrobe
http://localhost:8100/api/locations/
http://localhost:8100/api/locations/1/

POST
{
"closet_name": "closet2",
"section_number": 1,
"shelf_number": 1
}



http://localhost:8100/api/bins/
http://localhost:8100/api/bins/1/

POST
{
"closet_name": "closet2",
"bin_number": 2,
"bin_size": 2
}
