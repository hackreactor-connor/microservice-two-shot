from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'import_href',
        'closet_name',
        'id',
    ]



class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style_name',
        'color',
        'picture_url',
        'id',
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style_name',
        'color',
        'picture_url',
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }



@require_http_methods(['GET', "POST"])
def api_list_hats(request, location_id=None):
    if request.method == "GET":
        if location_id is not None:
            hats = Hat.objects.filter(location=location_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder = HatListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content['location'] = location # is this right?

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': 'invalid location id'},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder = HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {'message': 'invalid shoe id'},
                status=400,
            )
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
