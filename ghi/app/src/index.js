import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// async function loadHats() {
//   const res = await fetch('http://localhost:8090/api/hats/');
//   if (res.ok) {
//     console.log()
//     const data = await res.json();
//     root.render(
//       <React.StrictMode>
//         <App hats={data.hats} />
//       </React.StrictMode>
//     );
//   }
//   else {
//     console.error(res);
//   }
// }
// loadHats();


// async function loadShoes() {
//   const response = await fetch('http://localhost:8080/api/shoes/');
//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App shoes={data.shoes} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response);
//   }
// }
// loadShoes();




// holly fuck i have no clue how this works but it does, so dont toutch it unless you know what you're doing!!!!! PLEASE!!!!! this took me 3 hours to figure out

async function getData() {
  const resShoes = await fetch('http://localhost:8080/api/shoes/'); // this should with any number of endpoints, just add the fetch request, add the variable to the array, and add the corresponding data to the app component
  const resHats = await fetch('http://localhost:8090/api/hats/');

  Promise.all([resShoes, resHats])
    .then(values => Promise.all(values.map(value => value.json())))
    .then(finalVals => {
      let Shoes = finalVals[0];
      let Hats = finalVals[1];
      root.render(
        <React.StrictMode>
          <App shoes={Shoes.shoes} hats={Hats.hats} />
        </React.StrictMode>
      );
    })
}
getData();
