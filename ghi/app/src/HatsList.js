import React from "react";

function HatsList(props) {
    const deleteHat = async (id) => {
        fetch(`http://localhost:8090/api/hats/${id}/`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => {
            window.location.reload();
        })
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Location</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td width="16.66%"><img src={hat.picture_url} alt='' className="img-rounded" width="50%" height="50%" /></td>
                            <td width="16.66%">{hat.style_name}</td>
                            <td width="16.66%">{hat.color}</td>
                            <td width="16.66%">{hat.Fabric}</td>
                            <td width="16.66%">{hat.location}</td>
                            <td><button onClick={() => deleteHat(hat.id)} type="button" className="btn btn-dark">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;
